require('gsettings')

local handle_button = function(button, pressed)
    if (pressed) then
        local cmd = gsettings.dinput[button]
        if (type(cmd) == 'string') and (#cmd > 0) then
            if cmd:startswith('/sendkey') then
                AshitaCore:GetChatManager():QueueCommand(1, cmd..' down')
                AshitaCore:GetChatManager():QueueCommand(1, cmd..' up')
            else
                AshitaCore:GetChatManager():QueueCommand(1, cmd)
            end
            return true
        end
    end
    return false
end

buttons = {
    [32] = function(e)
        if (e.state == 0) then
            if handle_button('Up', true) then
                e.blocked = true
            end
        elseif (e.state == 9000) then
            if handle_button('Right', true) then
                e.blocked = true
            end
        elseif (e.state == 18000) then
            if handle_button('Down', true) then
                e.blocked = true
            end
        elseif (e.state == 27000) then
            if handle_button('Left', true) then
                e.blocked = true
            end
        end
    end,

    [48] = function(e)
        if handle_button('Square', e.state == 128) then
            e.blocked = true
        end
    end,

    [49] = function(e)
        if handle_button('Cross', e.state == 128) then
            e.blocked = true
        end
    end,

    [50] = function(e)
        if handle_button('Circle', e.state == 128) then
            e.blocked = true
        end
    end,

    [51] = function(e)
        if handle_button('Triangle', e.state == 128) then
            e.blocked = true
        end
    end,

    [52] = function(e)
        if handle_button('L1', e.state == 128) then
            e.blocked = true
        end
    end,

    [53] = function(e)
        if handle_button('R1', e.state == 128) then
            e.blocked = true
        end
    end,

    [54] = function(e)
        if handle_button('L2', e.state == 128) then
            e.blocked = true
        end
    end,

    [55] = function(e)
        if handle_button('R2', e.state == 128) then
            e.blocked = true
        end
    end,

    [56] = function(e)
        if handle_button('Share', e.state == 128) then
            e.blocked = true
        end
    end,

    [57] = function(e)
        if handle_button('Options', e.state == 128) then
            e.blocked = true
        end
    end,

    [58] = function(e)
        if handle_button('L3', e.state == 128) then
            e.blocked = true;
        end
    end,

    [59] = function(e)
        if handle_button('R3', e.state == 128) then
            e.blocked = true;
        end
    end,

    [60] = function(e)
        if handle_button('Playstation', e.state == 128) then
            e.blocked = true
        end
    end,

    [61] = function(e)
        if handle_button('Touchpad', e.state == 128) then
            e.blocked = true
        end
    end,
}

return buttons
