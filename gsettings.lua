require('common')

gsettings = T{}

function default_settings()
    local defaultSettings = T{
        dinput = T{
            Up = '',
            Down = '',
            Left = '',
            Right = '',
            Square = '',
            Cross = '',
            Circle = '',
            Triangle = '',
            L1 = '',
            R1 = '',
            L2 = '',
            R2 = '',
            L3 = '/sendkey NUMPAD7',
            R3 = '/sendkey NUMPAD*',
            Playstation = '/sendkey F8',
            Share = '',
            Options = '',
            Touchpad = '/jump',
        },
        xinput = T{
            Up = '',
            Down = '',
            Left = '',
            Right = '',
            X = '',
            A = '',
            B = '',
            Y = '',
            L1 = '',
            R1 = '',
            L2 = '',
            R2 = '',
            L3 = '/sendkey NUMPAD7',
            R3 = '/sendkey NUMPAD*',
            XBox = '/sendkey F8',
            View = '',
            Menu = '',
            Share = '/jump',
        }
    };

    return defaultSettings
end

function update_settings(settings)
    gsettings = settings
end
