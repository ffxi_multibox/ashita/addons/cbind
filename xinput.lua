require('gsettings')

local handle_button = function(button, pressed)
    if (pressed) then
        local cmd = gsettings.xinput[button]
        if (type(cmd) == 'string') and (#cmd > 0) then
            if cmd:startswith('/sendkey') then
                AshitaCore:GetChatManager():QueueCommand(1, cmd..' down')
                AshitaCore:GetChatManager():QueueCommand(1, cmd..' up')
            else
                AshitaCore:GetChatManager():QueueCommand(1, cmd)
            end
            return true
        end
    end
    return false
end

buttons = {
    [0] = function(e)
        if handle_button('Up', e.state == 1) then
            e.blocked = true
        end
    end,

    [1] = function(e)
        if handle_button('Down', e.state == 1) then
            e.blocked = true
        end
    end,

    [2] = function(e)
        if handle_button('Left', e.state == 1) then
            e.blocked = true
        end
    end,

    [3] = function(e)
        if handle_button('Right', e.state == 1) then
            e.blocked = true
        end
    end,

    [4] = function(e)
        if handle_button('Menu', e.state == 1) then
            e.blocked = true
        end
    end,

    [5] = function(e)
        if handle_button('View', e.state == 1) then
            e.blocked = true
        end
    end,

    [6] = function(e)
        if handle_button('L3', e.state == 1) then
            e.blocked = true
        end
    end,

    [7] = function(e)
        if handle_button('R3', e.state == 1) then
            e.blocked = true
        end
    end,

    [8] = function(e)
        if handle_button('L1', e.state == 1) then
            e.blocked = true
        end
    end,

    [9] = function(e)
        if handle_button('R1', e.state == 1) then
            e.blocked = true
        end
    end,

    [12] = function(e)
        if handle_button('A', e.state == 1) then
            e.blocked = true
        end
    end,

    [13] = function(e)
        if handle_button('B', e.state == 1) then
            e.blocked = true
        end
    end,

    [14] = function(e)
        if handle_button('X', e.state == 1) then
            e.blocked = true
        end
    end,

    [15] = function(e)
        if handle_button('Y', e.state == 1) then
            e.blocked = true
        end
    end,

    [32] = function(e)
        if handle_button('L2', e.state > 0) then
            e.blocked = true
        end
    end,

    [33] = function(e)
        if handle_button('R2', e.state > 0) then
            e.blocked = true
        end
    end,
}

return buttons
