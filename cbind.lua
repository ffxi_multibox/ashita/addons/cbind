addon.name      = 'cbind'
addon.author    = 'bambooya'
addon.version   = '1.0'
addon.desc      = 'Configurable xinput/dinput controller bindings'
addon.link      = 'https://gitlab.com/ffxi_multibox/ashita/addons/cbind.git'

require('common')

settings = require('settings');
dinput = require('dinput')
xinput = require('xinput')
require('gsettings')

ashita.events.register('load', 'cbind_load_callback', function ()
    update_settings(settings.load(default_settings()))
    settings.register('settings', 'settings_update', function(newSettings)
        update_settings(newSettings)
    end);
end)

ashita.events.register('unload', 'cbind_unload_callback', function ()
    settings.save('settings')
end)

ashita.events.register('dinput_button', 'cbind_dinput_button_callback', function (e)
    --[[ Valid Arguments
        e.button    - The controller button id.
        e.state     - The controller button state value.
        e.blocked   - Flag that states if the button has been, or should be, blocked.
        e.injected  - (ReadOnly) Flag that states if the button was injected by Ashita or an addon/plugin.
    --]]
    local button = dinput[e.button]
    if (type(button) == 'function') then
        button(e)
    end
end)

ashita.events.register('xinput_button', 'cbind_xinput_button_callback', function (e)
    --[[ Valid Arguments
        e.button    - The controller button id.
        e.state     - The controller button state value.
        e.blocked   - Flag that states if the button has been, or should be, blocked.
        e.injected  - (ReadOnly) Flag that states if the button was injected by Ashita or an addon/plugin.
    --]]
    local button = xinput[e.button]
    if (type(button) == 'function') then
        button(e)
    end
end)
